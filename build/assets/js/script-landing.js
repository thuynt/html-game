/*!
 * thuynt
 * 
 * 
 * @author thuynt
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */$(function () {
  var musicBackground = document.getElementById('music');
  var musicPlay = true;

  function musicSwitch() {
    $(this).toggleClass('on');

    if (musicPlay) {
      musicBackground.pause();
    } else {
      musicBackground.play();
    }

    musicPlay = !musicPlay;
  }

  $('#sound').click(musicSwitch);
});