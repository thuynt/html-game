
$(function(){
    var musicBackground = document.getElementById('music');
    var musicPlay = true;

    function musicSwitch(){
        $(this).toggleClass('on')
        if(musicPlay){
            musicBackground.pause();
        }
        else{
            musicBackground.play();
        }
        musicPlay=!musicPlay;
    }
    $('#sound').click(musicSwitch);
});