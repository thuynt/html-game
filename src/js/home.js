function getOffset(el) {
const rect = el.getBoundingClientRect();
return {
    left: rect.left + window.scrollX,
    top: rect.top + window.scrollY
};
}

$(document).ready(function () {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


    jQuery("#video_modal").on('shown.bs.modal', function() {
        if(typeof player.playVideo == 'function') {
            player.playVideo();
        } else {
            var fn = function(){
                player.playVideo();
            };
            setTimeout(fn, 200);
        }
    });
    jQuery("#video_modal").on('hidden.bs.modal', function() {
        player.stopVideo();
    });

    $(".icon-menu").click(function () {
        $(".lst-menu").toggleClass("active");
        $(this).toggleClass("close");
    });

    $(".img-group-ball").click(function () {
        $(".lst-team").addClass("active");
    });


    //responsive bóng khi lăn màn 1023
    if ($(window).width() < 1023) {
        $(window).scroll(function () {
            if ($('#offset1').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset1')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset2')[0]).top) {
                    if(scroll_offset_checkpoints.includes(1)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(1);
                    $(".box-progress-map").addClass("active");
                    $(".ball-white-icon").css("animation", "spin 2s linear infinite");
                    $(".ball-main-white").css({
                        "transform": "translate3d(-60.421875vw, 86.828125vw, 9.765625vw)",
                        "transition-duration": '3s'
                    });

                } else {}
            }

            if ($('#offset2').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset2')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset3')[0]).top) {
                    if(scroll_offset_checkpoints.includes(2)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(2);
                    $(".ball-main-white").css({
                        "transform": "translate3d(0, 180vw, 9.765625vw)",
                        "transition-duration": '3s'
                    });
                } else {}
            }

            if ($('#offset3').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset3')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset4')[0]).top) {
                    if(scroll_offset_checkpoints.includes(3)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(3);
                    $(".ball-main-white").css({
                        "transform": "translate3d(-60.421875vw, 290vw, 9.765625vw)",
                        "transition-duration": '3s'
                    });

                    $(".img-group-ball").addClass("active");
                } else {}
            }

            if ($('#offset4').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset4')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset5')[0]).top) {
                    if(scroll_offset_checkpoints.includes(4)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(4);
                    $(".ball-main-white").css({
                        "transform": "translate3d(0, 400vw, 9.765625vw)",
                        "transition-duration": '3s'
                    });
                    $(".lst-team").addClass("active");
                    $(".lst-partner").addClass("active");
                } else {}
            }

            if ($('#offset5').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset5')[0]).top) {
                    if(scroll_offset_checkpoints.includes(5)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(5);
                    $(".ball-main-white").css({
                        "transform": "translate3d(0, 470vw, 9.765625vw)",
                        "transition-duration": '2s'
                    });

                } else {}
            }
        });
    }
    else {
        console.log("width large");
    }
    //responsive bóng khi lăn màn 10231023
    //responsive bóng khi lăn màn 1365
    if ($(window).width() < 1365) {
        $(window).scroll(function () {
            if ($('#offset1').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset1')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset2')[0]).top) {
                    if(scroll_offset_checkpoints.includes(1)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(1);
                    $(".box-progress-map").addClass("active");
                    $(".ball-white-icon").css("animation", "spin 2s linear infinite");
                    $(".ball-main-white").css({
                        "transform": "translate3d(-57.2916666667vw, 26.0416666667vw, 5.2083333333vw)",
                        "transition-duration": '3s'
                    });

                } else {}
            }

            if ($('#offset2').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset2')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset3')[0]).top) {
                    if(scroll_offset_checkpoints.includes(2)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(2);
                    $(".box-progress-map").addClass("active");
                    $(".ball-main-white").css({
                        "transform": "translate3d(0, 46.875vw, 5.2083333333vw)",
                        "transition-duration": '3s'
                    });
                } else {}
            }

            if ($('#offset3').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset3')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset4')[0]).top) {
                    if(scroll_offset_checkpoints.includes(3)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(3);
                    $(".ball-main-white").css({
                        "transform": "translate3d(-57.2916666667vw, 72.9166666667vw, 5.2083333333vw)",
                        "transition-duration": '3s'
                    });

                    $(".img-group-ball").addClass("active");
                } else {}
            }

            if ($('#offset4').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset4')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset5')[0]).top) {
                    if(scroll_offset_checkpoints.includes(4)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(4);
                    $(".ball-main-white").css({
                        "transform": "translate3d(0, 104.166666667vw, 5.2083333333vw)",
                        "transition-duration": '3s'
                    });
                    $(".lst-team").addClass("active");
                    $(".lst-partner").addClass("active");
                } else {}
            }

            if ($('#offset5').length > 0) {
                if ($(this).scrollTop() + window_height > getOffset($('#offset5')[0]).top) {
                    if(scroll_offset_checkpoints.includes(5)){
                        console.log("không lăn nữa");
                        return ;
                    }
                    scroll_offset_checkpoints.push(5);
                    $(".ball-main-white").css({
                        "transform": "translate3d(0, 129.166666667vw, 5.2083333333vw)",
                        "transition-duration": '2s'
                    });

                } else {}
            }
        });
    }
    else {
        console.log("width large");
    }
    //responsive bóng khi lăn màn 1365





    var window_height = window.innerHeight;
    var scroll_offset_checkpoints = [];
    $(window).scroll(function () {
        if ($('#offset1').length > 0) {
            if ($(this).scrollTop() + window_height > getOffset($('#offset1')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset2')[0]).top) {
                if(scroll_offset_checkpoints.includes(1)){
                    console.log("không lăn nữa");
                    return ;
                }
                scroll_offset_checkpoints.push(1);
                $(".box-progress-map").addClass("active");
                $(".ball-white-icon").css("animation", "spin 2s linear infinite");
                $(".ball-main-white").css({
                    "transform": "translate3d(-1100px, 500px, 100px)",
                    "transition-duration": '3s'
                });

            } else {}
        }

        if ($('#offset2').length > 0) {
            if ($(this).scrollTop() + window_height > getOffset($('#offset2')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset3')[0]).top) {
                if(scroll_offset_checkpoints.includes(2)){
                    console.log("không lăn nữa");
                    return ;
                }
                scroll_offset_checkpoints.push(2);
                $(".ball-main-white").css({
                    "transform": "translate3d(0, 900px, 100px)",
                    "transition-duration": '3s'
                });
            } else {}
        }

        if ($('#offset3').length > 0) {
            if ($(this).scrollTop() + window_height > getOffset($('#offset3')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset4')[0]).top) {
                if(scroll_offset_checkpoints.includes(3)){
                    console.log("không lăn nữa");
                    return ;
                }
                scroll_offset_checkpoints.push(3);
                $(".ball-main-white").css({
                    "transform": "translate3d(-1100px, 1400px, 100px)",
                    "transition-duration": '3s'
                });

                $(".img-group-ball").addClass("active");
            } else {}
        }

        if ($('#offset4').length > 0) {
            if ($(this).scrollTop() + window_height > getOffset($('#offset4')[0]).top && $(this).scrollTop() + window_height <  getOffset($('#offset5')[0]).top) {
                if(scroll_offset_checkpoints.includes(4)){
                    console.log("không lăn nữa");
                    return ;
            }
                scroll_offset_checkpoints.push(4);
                $(".ball-main-white").css({
                    "transform": "translate3d(0, 2000px, 100px)",
                    "transition-duration": '3s'
                });
                $(".lst-team").addClass("active");
                $(".lst-partner").addClass("active");
            } else {}
        }

        if ($('#offset5').length > 0) {
            if ($(this).scrollTop() + window_height > getOffset($('#offset5')[0]).top) {
                if(scroll_offset_checkpoints.includes(5)){
                    console.log("không lăn nữa");
                    return ;
                }
                scroll_offset_checkpoints.push(5);
                $(".ball-main-white").css({
                    "transform": "translate3d(0, 2480px, 100px)",
                    "transition-duration": '2s'
                });

            } else {}
        }
    });
});


//chart js
var configd = {
    type: 'doughnut',
    data: {
        labelSmall:[
            '20% (10.000.000 token)',
            '5% (10.000.000 token)',
            '5% (500.000.000 token)',
            '5% (100.000.000 token)',
            '10% (100.000.000 token)',
            '10% (100.000.000 token)',
            '25% (100.000.000 token)',
            '5% (50.000.000 token)',
            '15% (25.000.000 token)',
        ],
        labels:['Team','Advisors','Partners', 'Private', 'Public','Marketing & Community', 'Ecosystem','DEX Liquidity','Play to earn'],
        datasets:[{
            data:[20, 5, 5, 5, 10, 10, 25, 5, 15],
            label:'',
            backgroundColor:['#00f6ff','#28e0e2','#2d4afe','#3fccfa','#0096d5', '#23c5fa','#005efa', '#05a4d8', '#141dd9'],
            borderWidth: 0,// division line width. (default=1)
        }],
    },//data setting
    options: {
        responsive: true,
        legend: {
            display: false
        },
        legendCallback: function (chart) {
            // Return the HTML string here.
            console.log(chart.data.datasets);
            var text = [];
            text.push('<ul class="' + chart.id + '-legend">');
            for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
                text.push('<li><span id="legend-' + i + '-item">');
                if (chart.data.labels[i]) {
                    text.push(chart.data.labels[i]);
                    text.push('<small id="small-' + i + '-item">');
                    if (chart.data.labelSmall[i]) {
                        text.push(chart.data.labelSmall[i]);
                    }
                    text.push('</small>');
                }
                text.push('</span></li>');
            }
            text.push('</ul>');
            return text.join("");
        },
        title: {
            display: false,
            text: 'Chart.js Doughnut Chart'
        },
        animation: {
            animateScale: true,
            animateRotate: true
        },
        cutoutPercentage: 60,
        tooltips: {
            mode: 'index',
            intersect: false,
            callbacks: {
                label: function (tooltipItem, data) {
                    let label = data.datasets[tooltipItem.datasetIndex].label + ' - ' + data.labels[tooltipItem.index];
                    let datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return label + ': ' + datasetLabel.toLocaleString();
                }
            }
        },
    }
};

var ctxd = document.getElementById('canvas').getContext('2d');

window.myDoughnut = new Chart(ctxd, configd);
$("#do_legend").html(window.myDoughnut.generateLegend());

// Show/hide chart by click legend
updateDataset = function (e, datasetIndex) {
    var index = datasetIndex;
    var ci = e.view.myDoughnut;
    var meta = ci.getDatasetMeta(0);
    var result= (meta.data[datasetIndex].hidden == true) ? false : true;
    if(result==true)
    {
        meta.data[datasetIndex].hidden = true;
        $('#' + e.path[0].id).css("text-decoration", "line-through");
    }else{
        $('#' + e.path[0].id).css("text-decoration","");
        meta.data[datasetIndex].hidden = false;
    }

    ci.update();
};
//function play video
var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: 'W6Z3DAnBC50',
    });
}